#!bin/bash

# Get Marketstack API key
[[ -z $MARKETSTACK_API_KEY ]] && echo "MARKETSTACK_API_KEY not set" && exit 1

readonly tmp_dir=download
readonly stocks_file=stocks.txt

mkdir -p $tmp_dir

# Calculate number of entries
readonly num_lines=$(wc -l < $stocks_file)

echo "Fetching $num_lines stocks"

# Fetch JSON for each currency pair
cat $stocks_file | while read token; do

  # Break out if there's a blank line
  [[ -z $token ]] && break
  
  output_file=$tmp_dir/$token.json
  echo Writing prices for $token to $output_file

  # Get as many recent data as we can from Marketstack using 1 hour intervals
  curl --silent "http://api.marketstack.com/v1/intraday?access_key=$MARKETSTACK_API_KEY&symbols=$token&limit=2000&interval=1hour" > $output_file

done
