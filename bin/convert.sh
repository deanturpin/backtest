#!bin/bash

# For each file in download directory, convert to csv
for file in download/*.json; do
  echo Converting $file
  cat $file | python3 bin/marketstack2csv.py > ${file/.json/.csv}
done

ls -l download/*.csv
