#!/bin/python3

import sys
import json
from dateutil import parser

# Read JSON from stdin
data = json.load(sys.stdin)

# Print header
print("time,open,high,low,close,volume")

# Print each data row
if "data" in data:

    # Marketstack returns data in reverse chronological order
    data["data"].reverse()

    # Print each row
    for row in data["data"]:

      # If any of the data are null, skip the row
      if row["close"] is None or row["volume"] is None:
          continue

      epoch = parser.parse(row["date"]).timestamp()

      print(int(epoch), end=",")
      print(float(row["open"]), end=",")
      print(float(row["high"]), end=",")
      print(float(row["low"]), end=",")
      print(float(row["close"]), end=",")
      print(float(row["volume"]))
