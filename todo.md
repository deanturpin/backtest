# Review

Review of the previous implementation of the backtester.

## What was good

- Nice seeing the profiling and benchmarking in the report

## What could be improved

- Not clear which were the backtests and the prospects
- Header file with the backtest lib was a bit weird
- Doxygen doesn't really add much
- Better (larger) test data
- The containers of doubles thing was an interesting idea but difficult to maintain; should return to traditional `struct` for a price point

## TODO

- print trade duration/count info
- saturating arithmetic operations
- move px calc to own routine
- <https://en.cppreference.com/w/cpp/utility/from_chars> (constexpr)
- zip profits and files / transform files into profits
- lerp function
- test parse errors in the CSV (e.g., only one cell in a row)
- use bind instead of lambdas
- move venv to Makefile
- use mkdocs [docker image](https://hub.docker.com/r/photoprism/mkdocs-material)
- consider spread
- Consider creating a `std::array` up front
- Ensure the size of the `struct` is optimal (e.g., check divisible by 2 in static assert)
- `environment: production`
- Using `- |` syntax for scripts
- VS Code dev extensions
- Fetch data from C++ (without curl/Python)
- Convert JSON to CSV within C++
- Would be nice to not have to expose the local file system (keep it in the container)
- LTO
- PGO

### Questions

- Is recent_dip a good test?
- Should we allow overlapping trades? Should the executor be making that decision?
- Why can't you test size of price pair? `assert(std::ranges::size(price_pair) == 2);`
- Why does MarketStack report null data for close and volume?

### Entry params

- Window (average window size)
- VWAP
- ATR
- Min price
- Max price
- Volume spike

### Exit params

- Take profit
- Stop loss

### Closing trades

Might be interesting to compare runs with and without overlapping trades: the strategy often fires many times in a row, but do we just want the first?

## Linux tuning

```bash
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors 
cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo performance | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor
```

## Compressed assets

```bash
find public -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec gzip -f -k {} \;
find public -type f -regex '.*\.\(htm\|html\|txt\|text\|js\|css\)$' -exec brotli -f -k {} \;
```
