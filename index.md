[![](https://gitlab.com/deanturpin/backtest/badges/main/pipeline.svg)](https://gitlab.com/deanturpin/backtest/-/pipelines)

See the source on [GitLab](https://gitlab.com/deanturpin/backtest/-/tree/main/src).

I started with the intention of using the latest tool chain: so Ubuntu 24.04, gcc from source and CMake 3.28 (with built-in modules support). I wanted to make everything as `constexpr` as possible from the outset and also C++ modules. However, you do spend a lot of time seeing compiler bugs! Normally that would be pretty cool but not when you can't debug it without a fight and/or workaround.

## Features of note

- CMake 3.28 with module support
- Extensive use of ranges and views
- Including `std::ranges::to` to convert views to containers
- `<print>` header and `std::println`
- `std::execution::par` for parallelism
- `constexpr` all the things
- `mold` linker
- `static_assert` for compile-time unit testing
- `sloccount` to estimate how much this would cost to develop

## Strategy

- Fetch stock prices via [MarketStack](https://marketstack.com/)
- Find all entries for the historical data
- Backtest the strategy and exercise all permutations of strategy parameters
- Report the best set of parameters
- Make predictions for the most recent data

## constexpr

It was painful to get right, not helped by errors from the Moon and the compiler blowing up. It's much less of a headache if you break the transform functions out into lambdas. However, once you get there, many classic undefined behaviours are caught at compile time: array out of bounds, invalid iterators, etc. Very cool.

Printing isn't `constexpr`, of course, but you can add runtime sections to your compile-time code.

```c++
if (not std::is_constant_evaluated())
  std::println("parsed: {} ", line);
```

I've added a module `constd.cxx` where I've put all the `constexpr` implementation of functions that are missing from the Standard Library. I've used the namespace `constd` and attempted to keep the API as close to the Standard Library as possible; which will make it easier to replace when the Standard Library catches up.

### Example errors from the Moon

```cpp
FAILED: CMakeFiles/backtest.dir/main.cxx.o 
/usr/local/bin/c++   -std=c++23 -O1 -Wall -Wextra -Wpedantic -fmodules-ts -march=native -std=gnu++23 -MD -MT CMakeFiles/backtest.dir/main.cxx.o -MF CMakeFiles/backtest.dir/main.cxx.o.d -fmodules-ts -fmodule-mapper=CMakeFiles/backtest.dir/main.cxx.o.modmap -MD -fdeps-format=p1689r5 -x c++ -o CMakeFiles/backtest.dir/main.cxx.o -c /home/deanturpin/backtest/src/main.cxx
/home/deanturpin/backtest/src/main.cxx: In instantiation of ‘<lambda(auto:55&&)> [with auto:55 = std::ranges::subrange<std::counted_iterator<std::ranges::transform_view<std::ranges::transform_view<std::ranges::filter_view<std::ranges::drop_view<std::ranges::split_view<std::basic_string_view<char>, std::ranges::single_view<char> > >, backtest(std::string_view)::<lambda(auto:56)> >, <lambda(auto:52&&)> >, <lambda(auto:54&&)> >::_Iterator<false> >, std::default_sentinel_t, std::ranges::subrange_kind::sized>&]’:
...
```

## Ranges and views

Views lend themselves to many "tradey" applications: anything that involves moving a window around the data fits beautifully. And all the weird edge cases are handled by this data structure. And whilst it's nice to keep things lazy; but sometimes it's just easier to create a container from a view, especially if there's any filtering going on: see `std::ranges::to`.

## Modules

C++ modules are still very new and you can't used them with the headers -- which sadly was the first thing I tried (for quite a while) -- but now CMake has direct support and this is much less painful. So you do still get crazy errors and compiler barfs but you can always revert to the "old fashioned" way of doing things (`#include`).

A very cool thing about modules is not having to manage (synchronise) the header files: you just import the module.

## Threads

I'm using `std::execution::par` to provide parallelism at a file level; but it's not without grumbles from the compiler.

```bash
/usr/local/include/c++/14.0.1/pstl/algorithm_impl.h:3390:5: note: ‘#pragma message:  [Parallel STL message]: "Vectorized algorithm unimplemented, redirected to serial"’
 3390 |     _PSTL_PRAGMA_MESSAGE("Vectorized algorithm unimplemented, redirected to serial");
      |     ^~~~~~~~~~~~~~~~~~~~
```

---
