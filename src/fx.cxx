module;
#include <algorithm>
#include <cmath>
#include <cstddef>
#include <functional>
#include <ranges>
#include <span>
export module fx;

export namespace fx {
struct price_t {
  size_t timestamp_{};
  double open_{};
  double high_{};
  double low_{};
  double close_{};
  double volume_{};
  double padding0_{};
  double padding1_{};
};

// Check optimal structure size
static_assert(sizeof(price_t) == 64uz);

// Calculate VWAP rolling average
constexpr double to_vwap(std::span<const price_t> xs) noexcept {
  // Initialise cumulative volumes
  auto cumulative_tp_volume{0.0};
  auto cumulative_volume{0.0};

  // Calculate VWAP for each sliding window
  for (auto x : xs) {
    cumulative_tp_volume += x.open_ * x.volume_;
    cumulative_volume += x.volume_;
  }

  return cumulative_tp_volume / cumulative_volume;
}

static_assert(to_vwap(std::array{
                  price_t{1, 1.0, 1.0, 1.0, 1.0, 1.0},
                  price_t{2, 2.0, 2.0, 2.0, 2.0, 2.0},
                  price_t{3, 3.0, 3.0, 3.0, 3.0, 3.0},
                  price_t{4, 4.0, 4.0, 4.0, 4.0, 4.0},
              }) == 3.0);

} // namespace fx
