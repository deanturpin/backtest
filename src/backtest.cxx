module;
#include "test.h"
#include <algorithm>
#include <cassert>
#include <functional>
#include <ranges>
#include <string_view>
#include <vector>
export module bt;

import fx;
import constd;

// Take a row as a string and convert it to a range of doubles
constexpr auto row_to_cells = [](std::ranges::range auto &&row) noexcept {
  // Split the row on commas and convert each cell to a double
  return row | std::views::split(',') | std::views::transform([](auto cell) {
           return constd::stod(std::string_view{cell});
         });
};

// Take a range of values and convert it to a price point
constexpr auto cells_to_price = [](std::ranges::range auto &&in) {
  // Convert range to vector
  const auto vals = in | std::ranges::to<std::vector<double>>();

  // Extract the values to make a price point
  return fx::price_t{.timestamp_ = static_cast<size_t>(vals[0]),
                     .open_ = vals[1],
                     .high_ = vals[2],
                     .low_ = vals[3],
                     .close_ = vals[4],
                     .volume_ = vals[5]};
};

static_assert(cells_to_price(std::vector{0.0, 1.0, 2.0, 3.0, 4.0, 5.0})
                  .timestamp_ == 0uz);

// Check the entry criteria are met for a window of prices
constexpr auto is_entry = [](std::ranges::range auto &&win,
                             const double vwap_ratio) noexcept {
  // Note the final price is dropped in all calculations
  // This is the entry price: the one after you decide to enter
  auto long_window_size = std::size(win);
  auto short_window_size = std::size(win) / 2;

  // Calculate two VWAPs
  auto long_range = win | std::views::take(long_window_size - 1);
  auto short_range = long_range | std::views::drop(short_window_size);

  // Check the entry criteria
  return to_vwap(long_range) < to_vwap(short_range) / vwap_ratio;
};

export namespace bt {

// Take a csv and forward test the strategy
constexpr bool forward_test(std::string_view csv, const size_t window_size,
                              const double vwap_ratio) {

  using namespace std::views;

  // Take a csv as a string, split it into rows, drop the header
  auto rows = csv | split('\n') | drop(1) |
              filter([](auto row) { return not std::empty(row); });

  // Convert each row to price points
  auto px = rows | transform(row_to_cells) | transform(cells_to_price) |
            std::ranges::to<std::vector<fx::price_t>>();

  // Get the last window as a sub-range
  auto last_window = px | std::views::drop(std::size(px) - window_size);

  return is_entry(last_window, vwap_ratio);
}

static_assert(forward_test(test::csv, 100uz, 1.2) or true);

// Take a csv as a string and backtest the strategy
// Backtest by running over the price data, finding the entry timestamps and
// running until take profit is achieved
constexpr double backtest(std::string_view csv, const size_t window_size,
                          const double vwap_ratio) {

  using namespace std::views;

  // Take a csv as a string, split it into rows, drop the header
  auto rows = csv | split('\n') | drop(1) |
              filter([](auto row) { return not std::empty(row); });

  // Convert each row to price points
  auto px = rows | transform(row_to_cells) | transform(cells_to_price) |
            std::ranges::to<std::vector<fx::price_t>>();

  // The window defines far back we look to evaluate the entry criteria
  // E.g., the moving average window
  auto windows = px | slide(window_size) |
                 filter([&](auto win) { return is_entry(win, vwap_ratio); });

  // Convert to timestamps
  auto timestamps = windows | transform([](std::ranges::range auto &&win) {
                      auto entry = win.back();
                      return entry.timestamp_;
                    });

  auto previous_timestamp = 0uz;
  auto total_trades = 0uz;
  auto average_profit = 0.0;

  // Find each entry in the price data
  for (auto t : timestamps) {

    // Skip if we're already ahead of the previous close
    if (t < previous_timestamp)
      continue;

    // Find the first timestamp in the prices
    auto it =
        std::ranges::find_if(px, [&](auto p) { return p.timestamp_ == t; });

    // If the timestamp is not found we have a problem
    [[unlikely]] if (it == std::cend(px))
      if (not std::is_constant_evaluated())
        assert(false);

    // Run until the take profit is achieved
    auto op = it->open_;
    auto tp = op * 1.03;

    // Find exit
    auto exit = std::ranges::find_if(it, std::cend(px),
                                     [tp](auto p) { return p.close_ > tp; });

    if (exit != std::cend(px)) {
      // We have found an exit, calculate the profit
      auto profit = 100.0 * (exit->close_ - op) / op;

      // Update the previous timestamp
      previous_timestamp = exit->timestamp_;
      average_profit += profit;
    } else {
      // No exit, profit is what it is
      auto profit = 100.0 * (px.back().close_ - op) / op;

      // Update the previous timestamp
      previous_timestamp = px.back().timestamp_;
      average_profit += profit;
    }

    // Increment the total number of trades
    ++total_trades;
  }

  return total_trades == 0uz ? 0.0 : average_profit / total_trades;
}

// Don't care about the result, just that it compiles
static_assert(backtest(test::csv, 100uz, 1.2) > 0.0 or true);

} // namespace bt
