module;
#include <cmath>
#include <ranges>
#include <string>
#include <string_view>
export module constd;

// constexpr implementations that are missing from the Standard Library

export namespace constd {

// String to double: std::stod
// Obviously the Standard Library function likes to throw an exception but I've
// made malformed input return a value
constexpr double stod(const std::string_view str) noexcept {

  auto out{0.0};

  // Iterate over the digits, and accumulate the result
  // Divide by the distance to the decimal point
  for (auto c : str) {
    if (c == '.')
      continue;

    // Move the decimal point one place to the right
    out *= 10.0;

    // If the character is not a digit, skip it
    u_int8_t digit = c - '0';
    if (digit > 9)
      continue;

    // Add the digit to the result
    out += digit;
  }

  // Find the number of digits after the decimal point
  for (auto i{0.0}; auto c : str | std::views::reverse) {
    if (c == '.') {
      out = out / std::pow(10.0, i);
      break;
    }

    i += 1.0;
  }

  return out;
}

// Malformed input
static_assert(constd::stod("") == 0.0);
static_assert(constd::stod("a") == 0.0);
static_assert(constd::stod("10a") == 100.0);

// No decimal point
static_assert(constd::stod("0") == 0.0);
static_assert(constd::stod("1") == 1.0);
static_assert(constd::stod("303") == 303.0);
static_assert(constd::stod("1234567890") == 1234567890.0);

// Decimal point
static_assert(constd::stod("303.1") == 303.1);
static_assert(constd::stod("303.0") == 303.0);
static_assert(constd::stod("1.000") == 1.0);
static_assert(constd::stod("1.0001") == 1.0001);
static_assert(constd::stod("303.0001") == 303.0001);

// Shortcuts
static_assert(constd::stod("303.") == 303.0);
static_assert(constd::stod(".1") == 0.1);

} // namespace constd
