#include <algorithm>
#include <cstdlib>
#include <execution>
#include <filesystem>
#include <fstream>
#include <numeric>
#include <print>
#include <ranges>
#include <iostream>
#include <string>
#include <string_view>
#include <syncstream>
#include <format>
#include <vector>

import fx;
import constd;
import bt;

int main() {

  std::atexit([] { std::println("cya"); });
  std::println("hiya");

  // Get list of files to process and check it exists
  const auto p = std::filesystem::path{"download/"};

  if (not std::filesystem::exists(p)) {
    std::println("Path {} does not exist", std::string{p});
    return 0;
  }

  using namespace std::views;

  // Process each file in the directory, skipping non-csv files
  const auto files =
      std::filesystem::directory_iterator{"download/"} |
      filter([](auto f) { return f.path().extension() == ".csv"; }) |
      transform([](auto f) { return f.path().string(); }) |
      std::ranges::to<std::vector<std::string>>();

  std::println("Processing {} files", std::size(files));

  std::for_each(
      std::execution::par, std::cbegin(files), std::cend(files),
      [&](auto file) {
        // The best trade will float to the top
        struct trade_summary {
          std::string file_{};
          double profit_{};
          size_t window_size_{};
          double vwap_ratio_{};
        };

        auto profits = std::vector<trade_summary>{};

        // Open the file and read it in
        auto in = std::ifstream{file};
        auto csv = std::string{std::istreambuf_iterator<char>{in}, {}};

        // Backtest the strategy
        for (auto vwap_ratio = 0.7; vwap_ratio < 1.2; vwap_ratio += 0.005)
          for (auto ws = 2uz; ws < 150uz; ws += 2uz)
            profits.emplace_back(file, bt::backtest(csv, ws, vwap_ratio), ws,
                                 vwap_ratio);

        // Average profit over all files
        auto total =
            std::accumulate(std::cbegin(profits), std::cend(profits), 0.0,
                            [](auto acc, auto p) { return acc + p.profit_; }) /
            std::size(profits);

        // No trades no interest
        if (total > 0.0 or total < 0.0) {

          // Find max profit
          auto max_profit = std::ranges::max_element(
              profits, [](auto a, auto b) { return a.profit_ < b.profit_; });

          // Test the most recent windows with the ideal parameters
          auto buy = bt::forward_test(csv, max_profit->window_size_,
                                      max_profit->vwap_ratio_);

          // Report top trade params
          if (buy)
            std::osyncstream(std::cout) << std::format("{:.1f}%\twin {}\tvwr {:.3f}\t{}\t{}\n",
                         max_profit->profit_, max_profit->window_size_,
                         max_profit->vwap_ratio_,
                         std::string_view{file}.substr(9), buy ? "BUY" : "");
        }
      });
}
