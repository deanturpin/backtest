all: download
	cmake -B build -S src -G Ninja
	cmake --build build --parallel
	time build/backtest

entr:
	ls src/* | entr -cr make

clean:
	$(RM) -r build/ download/

download:
	bash bin/marketstack.sh
	bash bin/convert.sh

stats:
	sloccount bin/ src/*.cxx src/CMakeLists.txt Makefile | grep -E "SLOC|Cost"

